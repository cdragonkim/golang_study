package main

import "fmt"

func main() {
	var num1 uint8 = 3
	var num2 uint8 = 2
	
	fmt.Println(num1 + num2)
	fmt.Println(num1 - num2)
	fmt.Println(num1 * num2)
	fmt.Println(num1 - num2)
	fmt.Println(num1 / num2)
	fmt.Println(num1 % num2)
	fmt.Println(num1 << num2)
	fmt.Println(num1 >> num2)
	fmt.Println(^num1)
	fmt.Println("-------")
	
	
	var num3 int = 3
	var num4 float32 = 2.2
	
	// fmt.Println(num3 + num4)
	fmt.Println(float32(num3) + num4)
	fmt.Println(num3 + int(num4))

	var num5 int16 = 10
	var num6 int32 = 80000
	
	fmt.Println("-------")
	fmt.Println(num5 + int16(num6))
}
